# -*- coding: utf-8 -*-

from jinja2 import Environment, FileSystemLoader
from from_xml_to_data import analyze_all_paths

def generate_html():
    env = Environment(loader=FileSystemLoader(''))
    template = env.get_template('template.html')
    year_data = minimize_data()
    return template.render(year_data=year_data)

def minimize_data(weather_data = analyze_all_paths()):

    # [data, year, max_rain, max_temperature, week_data,...]
    year_data = []
    max_rain_overall = 0.0
    weekly_max_rain_overall = 0.0
    max_temperature_overall = 0.0
    min_temperature_overall = 0.0
    weekly_max_avg_temperature_overall = 0.0
    weekly_min_avg_temperature_overall = 0.0

    for data, year, max_rain, max_temperature, min_temperature, week_data in weather_data:
        # max rain over years
        if max_rain > max_rain_overall:
            max_rain_overall = max_rain
        for week, rain_amount, temperature in week_data:
            if rain_amount > weekly_max_rain_overall:
                weekly_max_rain_overall = rain_amount

        # max and min temperature over years
        if max_temperature > max_temperature_overall:
            max_temperature_overall = max_temperature
        if min_temperature < min_temperature_overall:
            min_temperature_overall = min_temperature
        for week, rain_amount, temperature in week_data:
            if temperature > weekly_max_avg_temperature_overall:
                weekly_max_avg_temperature_overall = temperature
            if temperature < weekly_min_avg_temperature_overall:
                weekly_min_avg_temperature_overall = temperature


    for data, year, max_rain, max_temperature, min_temperature, week_data in weather_data:

        #daily
        daily_rain_amount_in_rgb_data = []
        for date, rain_amount, temperature in data:
            # rgb from 255, 255, 255 to 0, 162, 255 = diff(255, 93, 0)
            ratio = rain_amount / max_rain_overall
            if rain_amount == 0.0:
                rain_amount_in_rgb = "255,255,255"
            else:
                r = str(255 - int(255 * ratio))
                g = str(255 - int(93 * ratio))
                b = "255"
                rain_amount_in_rgb = "{0},{1},{2}".format(r, g, b)
            daily_rain_amount_in_rgb_data.append((date, rain_amount, rain_amount_in_rgb))

        daily_temperature_in_rgb_data = []
        for date, rain_amount, temperature in data:
            # rgb from cold 151,207,240 to zero 255,255,255 to warm 252,206,111 to hot 234,127,111
            max_ratio = temperature / max_temperature_overall
            min_ratio = temperature / min_temperature_overall
            if temperature == 0.0:
                temperature_in_rgb = "255,255,255"
            elif temperature <= 0.0:
                # rgb from cold 151,207,240
                r = str(255 - int((255-151) * min_ratio))
                g = str(255 - int((255-207) * min_ratio))
                b = str(255 - int((255-240) * min_ratio))
                temperature_in_rgb = "{0},{1},{2}".format(r, g, b)
            elif 0.0 < temperature <= 24.0:
                # rgb from warm 252,206,111
                r = str(255 - int((255-252) * (temperature / 24.0)))
                g = str(255 - int((255-206) * (temperature / 24.0)))
                b = str(255 - int((255-111) * (temperature / 24.0)))
                temperature_in_rgb = "{0},{1},{2}".format(r, g, b)
            elif temperature > 24.0:
                # rgb from hot 234,127,111
                r = str(255 - int((252-234) * (temperature / 24.0)))
                g = str(255 - int((206-127) * (temperature / 24.0)))
                b = "144"
                temperature_in_rgb = "{0},{1},{2}".format(r, g, b)
            else:
                temperature_in_rgb = "0,100,200"
            daily_temperature_in_rgb_data.append((date, temperature, temperature_in_rgb))



        #weekly
        weekly_rain_amount_in_rgb_data = []
        for week, rain_amount, temperature in week_data:
            if rain_amount == 0.0:
                rain_amount_in_rgb = "255,255,255"
            else:
                # rgb from 255, 255, 255 to 0, 162, 255 = diff(255, 93, 0)
                ratio = rain_amount / weekly_max_rain_overall
                r = str(255 - int(255 * ratio))
                g = str(255 - int(93 * ratio))
                b = "255"
                rain_amount_in_rgb = "{0},{1},{2}".format(r, g, b)
            weekly_rain_amount_in_rgb_data.append((week, rain_amount, rain_amount_in_rgb))

        weekly_temperature_in_rgb_data = []
        for week, rain_amount, temperature in week_data:
            # rgb from cold 151,207,240 to zero 255,255,255 to warm 252,206,111 to hot 234,127,111
            max_ratio = temperature / max_temperature_overall
            min_ratio = temperature / min_temperature_overall
            if temperature == 0.0:
                temperature_in_rgb = "255,255,255"
            elif temperature <= 0.0:
                # rgb from cold 151,207,240
                r = str(255 - int((255-151) * min_ratio))
                g = str(255 - int((255-207) * min_ratio))
                b = str(255 - int((255-240) * min_ratio))
                temperature_in_rgb = "{0},{1},{2}".format(r, g, b)
            elif 0.0 < temperature <= 24.0:
                # rgb from warm 252,206,111
                r = str(255 - int((255-252) * (temperature / 24.0)))
                g = str(255 - int((255-206) * (temperature / 24.0)))
                b = str(255 - int((255-111) * (temperature / 24.0)))
                temperature_in_rgb = "{0},{1},{2}".format(r, g, b)
            elif temperature > 24.0:
                # rgb from hot 234,127,111
                r = str(255 - int((252-234) * (temperature / 24.0)))
                g = str(255 - int((206-127) * (temperature / 24.0)))
                b = "144"
                temperature_in_rgb = "{0},{1},{2}".format(r, g, b)
            else:
                temperature_in_rgb = "0,100,200"
            weekly_temperature_in_rgb_data.append((week, temperature, temperature_in_rgb))

        year_data.append((year, daily_rain_amount_in_rgb_data, weekly_rain_amount_in_rgb_data, daily_temperature_in_rgb_data, weekly_temperature_in_rgb_data))
    return year_data

with open("public/index.html", "wb") as f:
    f.write(generate_html())

# with open("data.txt", "wb") as af:
#     for year, rain_amount_in_rgb_data in year_data:
#         af.write(str(year))
#         for rain_amount_in_rgb in rain_amount_in_rgb_data:
#             af.write(rain_amount_in_rgb)
