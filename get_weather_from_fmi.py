# -*- coding: utf-8 -*-
import urllib
import time
from config import API_KEY

max_year = 2016
min_year = 1988

for i in range(max_year-min_year):
    url = "http://data.fmi.fi/fmi-apikey/{0}/wfs?request=getFeature&storedquery_id=fmi::observations::weather::daily::simple&place=kaisaniemi,Helsinki&starttime={1}-01-01T00:00:00Z&endtime={2}-01-01T00:00:00Z".format(API_KEY, min_year + i, min_year + 1 + i)
    path = "/data/kaisaniemi_weather{0}.xml".format(min_year + i)
    urllib.urlretrieve(url, path)
