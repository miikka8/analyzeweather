# -*- coding: utf-8 -*-

import glob
import xml.etree.ElementTree as ET

def analyze_all_paths(weather_file_paths = glob.glob('data/*.xml')):
    weather_data = []
    for file_path in weather_file_paths:
        print file_path
        xml_root = generate_xml_tree(file_path)
        file_weather = get_days_rain_tmp_from_xml(xml_root)
        print file_weather[2], file_weather[3]
        weather_data.append(file_weather)
    return weather_data

def generate_xml_tree(file_path):
    tree = ET.parse(file_path)
    xml_root = tree.getroot()
    return xml_root

def get_days_rain_tmp_from_xml(xml_root):
    data = []
    year = xml_root[0][0][1].text[:4]
    max_rain = 0.0
    max_temperature = 0.0
    min_temperature = 0.0
    rain_amount = 0.0
    temperature = 0.0
    previous_date = ""

    for xml_item in range(len(xml_root)):
        date = xml_root[xml_item][0][1].text[:10]
        item_name = xml_root[xml_item][0][2].text
        item_value = xml_root[xml_item][0][3].text

        if item_name == "rrday":
            rain_amount = float(item_value)
            if rain_amount == -1.0:
                rain_amount = 0.0
            if rain_amount > max_rain:
                max_rain = rain_amount

        if item_name == "tday":
            temperature = float(item_value)
            if temperature > max_temperature:
                max_temperature = temperature
            if temperature < min_temperature:
                min_temperature = temperature

        if date != previous_date:
            data.append((date, rain_amount, temperature))

        previous_date = date
    week_data = weeks(data)
    return (data, year, max_rain, max_temperature, min_temperature, week_data)

def weeks(data):
    week_data = []
    week_rain_sum = 0.0
    week_temperature_avg = 0.0
    day = 0
    week_number = 1
    for date, rain_amount, temperature in data:
        if day != 0 and day % 7 == 0:
            week_temperature_avg = week_temperature_avg / 7
            week_data.append((week_number, week_rain_sum, week_temperature_avg))
            week_rain_sum = 0
            week_temperature_avg = 0
            week_number += 1
        else:
            week_rain_sum += rain_amount
            week_temperature_avg += temperature
        day += 1
    return week_data

if __name__ == "__main__":
    # execute only if run as a script
    print analyze_all_paths()[0][0]
